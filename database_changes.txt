1. When sorting the database, the row identifier are rebuild.
So, before the sorting, the row identificator are equal to the row indentificator in the .csv file plus 1. The plus one refers to the variable row th>

2. Number of records are 93, so we have 93 cities.

3. Column variables

	From total to total3 = total		
	From total4 to total6 = ocupadas
	From total7 to total9 = não ocupadas
	
	Order
		Total
		Urbana
		Rural
4. Municipios
	A. Há dois "Rio de Janeiro"
		Rio de Janeiro
		Rio de Janeiro (RJ)
		
	B. Há municípios diferentes que coloquei o mesmo código do mcirc porque estou seguindo a padronização do ISP 2010 devido às áreas de delegacia
		1. Santo Antônio de Pádua;Aperibé - 9999993
		2. Comendador Levy Gasparian;Areal;Três Rios - 9999991
		3. Cabo Frio;Arraial do Cabo - 9999985
		4. Cardoso Moreira;Italva - 9999995
		5. Cordeiro;Macuco - 9999998
		6. Magé;Guapimirim - 9999982
		7. Itaboraí;Tanguá - 9999983
		8. Itaperuna;São José de Ubá - 9999996
		9. Miguel Pereira;Paty dos Alferes - 9999999
		10. Natividade;Varre-Sai - 9999997
		11. Porto Real;Quatis - 9999992
		12. Quissamã;Carapebus - 9999994
		13. Santo Antônio de Pádua;Aperibé - 9999993
		
	C. Merging different names
		I. 2010
			a1. Aperibé (RJ) => Santo Antônio de Pádua;Aperibé
				Santo Antônio de Pádua (RJ) => Santo Antônio de Pádua;Aperibé
				
			a2. Areal (RJ) => Comendador Levy Gasparian;Areal;Três Rios
				Comendador Levy Gasparian (RJ) => Comendador Levy Gasparian;Areal;Três Rios
				 
				Três Rios (RJ) => Comendador Levy Gasparian;Areal;Três Rios
				
			a3. Arraial do Cabo (RJ) => Cabo Frio;Arraial do Cabo
			a4. Carapebus (RJ) => Macaé
				Quissamã (RJ) => Macaé
				Macaé (RJ) => Macaé
				9999994 => 3302403
			a5. Guapimirim (RJ) => Magé;Guapimirim
			a6. Italva (RJ) => Cardoso Moreira
				Itaperuna (RJ) => Cardoso Moreira
				São José de Ubá (RJ) => Cardoso Moreira
				Cardoso Moreira (RJ) => Cardoso Moreira
				 9999996 => 9999995
			a7. Macuco (RJ) => Cordeiro;Macuco
				Cordeiro (RJ) => Cordeiro;Macuco
			a8. Miguel Pereira (RJ) => Miguel Pereira;Paty dos Alferes
				Paty do Alferes (RJ) => Miguel Pereira;Paty dos Alferes
			a9. Natividade (RJ) => Natividade;Varre-Sai
				Varre-Sai (RJ) => Natividade;Varre-Sai
			a10. Porto Real (RJ) => Porto Real;Quatis
				Quatis (RJ) => Porto Real;Quatis
			
			a11. Tanguá (RJ) => Itaboraí;Tanguá
				Itaboraí (RJ) => Itaboraí;Tanguá
			a12. Campos dos Goytacazes (RJ) => Campos dos Goytacazes
				São Francisco de Itabapoana (RJ) => Campos dos Goytacazes
					3304755 => 3301009
			a13. Queimados (RJ) => Japeri
				Japeri (RJ) => Japeri
				3304144 => 3302270
			a14. Pinheiral (RJ) => Pinheral
				Piraí (RJ) => Pinheral
				3304003 => 3303955

5. tabela1972-opr
			a1.	 Aperibé (RJ) => Santo Antônio de Pádua;Aperibé
					3300159 => 9999993
				Santo Antônio de Pádua (RJ) => Santo Antônio de Pádua;Aperibé
					3304706 => 9999993
								
			a2. 	Areal (RJ) => Comendador Levy Gasparian;Areal;Três Rios
					3300225 => 9999991
				Comendador Levy Gasparian (RJ) => Comendador Levy Gasparian;Areal;Três Rios
					3300951 => 9999991
				Três Rios (RJ) => Comendador Levy Gasparian;Areal;Três Rios
				 	3306008 => 9999991
				
			a3. 	Arraial do Cabo (RJ) => Cabo Frio;Arraial do Cabo
					3300258 <- 9999985
				Cabo Frio (RJ) <- Cabo Frio;Arraial do Cabo
					3300704 <- 9999985
				
			a4. 	Carapebus (RJ) => Macaé
					3300936 <- 3302403
				Quissamã (RJ) => Macaé
					3304151 <- 3302403
				Macaé (RJ) => Macaé
					9999994 <- 3302403
					
			a5. 	Guapimirim (RJ) => Magé;Guapimirim
					3301850 <- 9999982
					
			a6. 	Italva (RJ) => Cardoso Moreira
					3302056 <- 9999995
				Itaperuna (RJ) => Cardoso Moreira
					3302205 <- 9999995
				São José de Ubá (RJ) => Cardoso Moreira
					3305133 <- 9999995
				Cardoso Moreira (RJ) => Cardoso Moreira
					3301157 <- 9999995
				 
			a7. 	Macuco (RJ) => Cordeiro;Macuco
					3302452 <- 9999998
				Cordeiro (RJ) => Cordeiro;Macuco
					3301504 <- 9999998
					
			a8. 	Miguel Pereira (RJ) => Miguel Pereira;Paty dos Alferes
					3302908 <- 9999999
				Paty do Alferes (RJ) => Miguel Pereira;Paty dos Alferes
					3303856 <- 9999999
					
			a9. 	Natividade (RJ) => Natividade;Varre-Sai
					3303104 <- 9999997
				Varre-Sai (RJ) => Natividade;Varre-Sai
					3306156 <- 9999997
				
			a10. 	Porto Real (RJ) => Porto Real;Quatis
					3304110 <- 9999992				
				Quatis (RJ) => Porto Real;Quatis
					3304128 <- 9999992
			
			a11. 	Tanguá (RJ) => Itaboraí;Tanguá
					3305752 <- 9999983
				Itaboraí (RJ) => Itaboraí;Tanguá
					3301900 <- 9999983
				
			a12. 	Campos dos Goytacazes (RJ) => Campos dos Goytacazes
				São Francisco de Itabapoana (RJ) => Campos dos Goytacazes
					3304755 => 3301009
					
			a13. 	Queimados (RJ) => Japeri
					3304144 => 3302270
				Japeri (RJ) => Japeri
				
			a14. Pinheiral (RJ) => Pinheral
				Piraí (RJ) => Pinheral
				3304003 => 3303955

6. tabela200






		
